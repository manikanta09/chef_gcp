/*
This is a test server definition for GCE+Terraform for GH-9564
*/

provider "google" {
	credentials = "${file("vistadevops-3c603128d723.json")}"
	project = "vistadevops"
	region = "us-east1"
}

resource "google_compute_firewall" "gh-9564-firewall-externalssh" {
  name    = "gh-9564-firewall-externalssh"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["0-65535"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["externalssh"]
}

resource "google_compute_instance" "dev1" {
  name         = "gcp-rhel7-dev1-tf"
  machine_type = "g1-small"
  zone         = "us-central1-a"
  tags         = ["externalssh"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }

  network_interface {
    network = "default"

    access_config {
      # Ephemeral
    }
  }

  provisioner "remote-exec" {
    connection {
      host        = self.network_interface[0].access_config[0].nat_ip
      type        = "ssh"
      user        = "mani09"
      agent       = "false"
      timeout     = "800s"
      private_key = "${file("./mani09")}"
    }

    inline = [
      "sudo apt-get -y update" ,
# sudo apt install firewalld -y",
   #       "sudo firewall-cmd --zone=public --permanent --add-port=80/tcp  ",
   #          "sudo firewall-cmd --zone=public --permanent --add-port=5665/tcp" ,
   #          "sudo firewall-cmd --reload", 
          # to download the chef-server package,
          "wget https://packages.chef.io/files/stable/chef-server/12.18.14/ubuntu/18.04/chef-server-core_12.18.14-1_amd64.deb" ,
          # to install chef-server
          "sudo dpkg -i chef-server-core_*.deb",
          #to start chef server 
          "sudo chef-server-ctl reconfigure" ,
          "sudo apt-get install openssl -y",
          # Create the SSL Certificate
          "openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/ca.key -out /etc/ssl/certs/ca.crt",
         
          # create  an admin user
          "sudo chef-server-ctl user-create chefadmin Chef Admin chefadmin@gmail.com 'admin@123' --filename ./chfsrv.pem" ,
          # to view the list of users
          # chef-server-ctl user-list
          # create an organization
          "sudo chef-server-ctl org-create VBI 'vbi Pvt Ltd.' --association_user chefadmin --filename ./org.pem" ,
        # to view the list of organizations
        # chef-server-ctl org-list
        
          # to install the management console
            "sudo chef-server-ctl install chef-manage",
            "sudo chef-server-ctl reconfigure --accept-license",
            "sudo chef-manage-ctl reconfigure --accept-license",
         # to install reporting feature
          "sudo chef-server-ctl install opscode-reporting",
          "sudo chef-server-ctl reconfigure",
          "sudo opscode-reporting-ctl reconfigure --accept-license"
    ]
  }

  # Ensure firewall rule is provisioned before server, so that SSH doesn't fail.
  depends_on = ["google_compute_firewall.gh-9564-firewall-externalssh"]

  service_account {
    scopes = ["compute-ro"]
  }

  metadata = {
    ssh-keys = "mani09:${file("./mani09.pub")}"
  }
}
